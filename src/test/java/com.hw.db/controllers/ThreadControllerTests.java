package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

class ThreadControllerTests {
    private ThreadController threadController;
    private Thread slugThread;
    private Thread integerThread;
    private Thread newIntegerThread;
    private final String slug = "firstSlug";
    private final int integerId = 123;
    private final int newIntegerId = 456;

    private Post post;
    private final String name = "Java";
    private User user;
    private Vote vote;

    @BeforeEach
    void setUp() {
        slugThread = new Thread();
        slugThread.setSlug(slug);

        integerThread = new Thread();
        integerThread.setId(integerId);

        newIntegerThread = new Thread();
        newIntegerThread.setId(newIntegerId);

        threadController = new ThreadController();
        vote = new Vote(name, 1);

        post = new Post();
        post.setAuthor(name);

        user = new User();
        user.setNickname(name);
    }

    @Test
    @DisplayName("CheckIdOrSlug test with slug")
    void checkSlugWithSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(slugThread);
            assertEquals(slugThread, threadController.CheckIdOrSlug(slug));
        }
    }

    @Test
    @DisplayName("CheckIdOrSlug test with integer id")
    void checkSlugWithInteger() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(integerId)).thenReturn(integerThread);
            assertEquals(integerThread, threadController.CheckIdOrSlug(String.valueOf(integerId)));
        }
    }

    @Test
    @DisplayName("Post creation test")
    void checkPostCreation() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> posts = new ArrayList<>();
            posts.add(post);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(slugThread);
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(name)).thenReturn(user);
                assertEquals(slugThread, ThreadDAO.getThreadBySlug(slug));
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), threadController.createPost(slug, posts));
            }
        }
    }

    @Test
    @DisplayName("Check posts")
    void checkPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            final List<Post> posts = List.of(post);
            threadMock.when(() -> ThreadDAO.getThreadById(integerId)).thenReturn(integerThread);
            threadMock.when(() -> ThreadDAO.getPosts(integerId, 1, 20, null, false)).thenReturn(posts);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), threadController.Posts(String.valueOf(integerId), 1, 20, null, false));
        }
    }

    @Test
    @DisplayName("Check thread change")
    void checkThreadChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(integerId)).thenReturn(integerThread);
            threadMock.when(() -> ThreadDAO.getThreadById(newIntegerId)).thenReturn(newIntegerThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newIntegerThread), threadController.change(String.valueOf(newIntegerId), integerThread));
        }
    }

    @Test
    @DisplayName("Check info")
    void checkThreadInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(integerId)).thenReturn(integerThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(integerThread), threadController.info(String.valueOf(integerId)));
        }
    }

    @Test
    @DisplayName("Check vote creation")
    void checkCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                slugThread.setVotes(1);
                userMock.when(() -> UserDAO.Info(name)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(slugThread);
                threadMock.when(() -> ThreadDAO.change(vote, slugThread.getVotes())).thenReturn(slugThread.getVotes());
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(slugThread), threadController.createVote(slug, vote));
            }
        }

    }
}
